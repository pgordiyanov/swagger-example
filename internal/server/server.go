package server

import (
	"time"

	"swagger_example/internal/server/restapi"
	"swagger_example/internal/server/restapi/operations"
	"swagger_example/models"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
)

func NewRestApiServer() (*restapi.Server, error) {
	swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
	if err != nil {
		return nil, err
	}

	api := operations.NewTestServerAPI(swaggerSpec)
	configureEndpoints(api)
	svr := restapi.NewServer(api)

	svr.Port = 8081
	svr.ConfigureFlags()
	svr.ConfigureAPI()

	return svr, nil
}

func configureEndpoints(api *operations.TestServerAPI) {
	api.GetOrdersHandler = operations.GetOrdersHandlerFunc(func(params operations.GetOrdersParams) middleware.Responder {
		return operations.NewGetOrdersOK().WithPayload([]*models.Order{
			{
				ID:         1,
				Name:       "Test order",
				TimePlaced: strfmt.DateTime(time.Now()),
			},
		})
	})

	api.PostOrdersHandler = operations.PostOrdersHandlerFunc(func(params operations.PostOrdersParams) middleware.Responder {
		return operations.NewPostOrdersCreated().WithLocation(2)
	})
}
