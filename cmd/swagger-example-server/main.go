package main

import (
	"os"
	"os/signal"
	"swagger_example/internal/server"
)

func main() {
	s, err := server.NewRestApiServer()
	if err != nil {
		panic(err)
	}

	go s.Serve()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	<-stop
	s.Shutdown()
}
