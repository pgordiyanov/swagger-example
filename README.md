# Swagger-example

### Install binary

https://goswagger.io/install.html


### Generate server
Run from the root of the module 
```shell
swagger generate server -f ./internal/server/restapi/swagger.yaml -s ./internal/server/restapi --exclude-main
```
